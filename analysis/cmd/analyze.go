package main

import (
	"gitlab.com/nitronick600/siacorn/analysis/jsontag"
	"gitlab.com/nitronick600/siacorn/analysis/lockcheck"
	"gitlab.com/nitronick600/siacorn/analysis/responsewritercheck"
	"golang.org/x/tools/go/analysis/multichecker"
)

func main() {
	multichecker.Main(
		lockcheck.Analyzer,
		responsewritercheck.Analyzer,
		jsontag.Analyzer,
	)
}
