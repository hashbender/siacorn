package transactionpool

import "gitlab.com/nitronick600/siacorn/modules"

// Alerts implements the modules.Alerter interface for the transactionpool.
func (tpool *TransactionPool) Alerts() []modules.Alert {
	return []modules.Alert{}
}
