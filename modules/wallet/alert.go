package wallet

import (
	"gitlab.com/nitronick600/siacorn/modules"
)

// Alerts implements the Alerter interface for the wallet.
func (w *Wallet) Alerts() []modules.Alert {
	return []modules.Alert{}
}
